from pygame import Surface, draw

from data.utils import distanza_punti, erone


class Triangle:

    def __init__(self, vertici: tuple[tuple[int, int], tuple[int, int], tuple[int, int]], surface: Surface,
                 visibile: bool = False):
        self.__lato1 = None
        self.__lato2 = None
        self.__lato3 = None
        self.vertici = vertici
        self.area = self.calcola_area()

        if visibile:
            self.disegna(surface=surface)

    def lista_lati(self):
        return self.__lato1, self.__lato2, self.__lato3

    def calcola_lati(self):
        self.__lato1 = distanza_punti(self.vertici[0], self.vertici[1])
        self.__lato2 = distanza_punti(self.vertici[1], self.vertici[2])
        self.__lato3 = distanza_punti(self.vertici[2], self.vertici[0])

    def calcola_area(self):
        self.calcola_lati()
        return erone(self.__lato1, self.__lato2, self.__lato3)

    def disegna(self, surface, color=(125, 125, 125), width=1):
        draw.polygon(surface=surface, color=color, points=self.vertici, width=width)
