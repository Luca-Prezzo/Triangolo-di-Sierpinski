from pygame import draw, Surface


class Point:

    def __init__(self, posizione: tuple[int, int], surface: Surface, visibile: bool = False):
        self.posizione: tuple[int, int] = posizione
        self.visibile: bool = visibile
        self.colore: tuple[int, int, int] = (255, 0, 0)
        self.surface: Surface = surface

        if self.visibile:
            self.disegna()

    def disegna(self) -> None:
        draw.circle(surface=self.surface, color=self.colore,
                    center=self.posizione, radius=2)

