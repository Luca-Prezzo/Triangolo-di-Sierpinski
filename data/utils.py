from math import sqrt, pow
import pygame
from random import randint


def crea_finestra(width: int, height: int) -> pygame.Surface:
    pygame.display.set_caption("Triangolo di Sierpinski")
    return pygame.display.set_mode((width, height))


def controlla_eventi(eventi):
    return all(event.type != pygame.QUIT for event in eventi)


def posizione_casuale(width: int) -> tuple[int, int]:
    return randint(0, width), randint(0, width)


def altezza_schermo(margine: float = 0.05) -> int:
    larghezza = pygame.display.Info().current_h
    return round(larghezza - (larghezza * margine))


def calcola_punto_medio(posizione, vertice):
    posizione = list(posizione)
    distanza_x_pv = abs(posizione[0] - vertice[0])
    distanza_y_pv = abs(posizione[1] - vertice[1])

    if vertice[0] > posizione[0]:
        posizione[0] += distanza_x_pv / 2
    else:
        posizione[0] -= distanza_x_pv / 2

    if vertice[1] > posizione[1]:
        posizione[1] += distanza_y_pv / 2
    else:
        posizione[1] -= distanza_y_pv / 2

    return tuple(posizione)


def distanza_punti(vertice_a, vertice_b):
    return sqrt(pow(abs(vertice_a[0] - vertice_b[0]), 2) + pow(abs(vertice_a[1] - vertice_b[1]), 2))


def erone(lato_a, lato_b, lato_c):
    perimetro = lato_a + lato_b + lato_c
    semiperimetro = perimetro / 2
    valore_erone = semiperimetro * (semiperimetro - lato_a) * (semiperimetro - lato_b) * (semiperimetro - lato_c)
    return sqrt(abs(valore_erone))
