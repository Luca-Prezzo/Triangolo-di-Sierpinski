from random import choice
import pygame
import data.utils
from data.point import Point
from data.triangle import Triangle


def main():
    # faccio init e prendo i dati dallo schermo per creare la finestra quadrata
    pygame.init()
    larghezza = altezza = data.utils.altezza_schermo()
    finestra = data.utils.crea_finestra(larghezza, altezza)

    # salvo i vertici per il triangolo originale con un margine calcolato al 2%
    margine = larghezza * 0.02
    vertice_a = Point(posizione=(int(larghezza // 2), int(margine)), surface=finestra, visibile=True)
    vertice_b = Point(posizione=(int(larghezza - margine), int(altezza - margine)), surface=finestra, visibile=True)
    vertice_c = Point(posizione=(int(margine), int(altezza - margine)), surface=finestra, visibile=True)

    # creo il clock per il framerate, il set dei punti
    clock = pygame.time.Clock()

    # creo il triangolo di base
    triangolo_origine: Triangle = Triangle((vertice_a.posizione, vertice_b.posizione, vertice_c.posizione), finestra,
                                           False)

    # controllo gli eventi per gestire l'uscita
    while data.utils.controlla_eventi(pygame.event.get()):

        # creo il nuovo punto con posizione casuale
        punto = Point(posizione=data.utils.posizione_casuale(larghezza), surface=finestra, visibile=False)
        triangolo_pab = Triangle(vertici=(punto.posizione, vertice_a.posizione, vertice_b.posizione), surface=finestra,
                                 visibile=False)
        triangolo_pbc = Triangle(vertici=(punto.posizione, vertice_b.posizione, vertice_c.posizione), surface=finestra,
                                 visibile=False)
        triangolo_pca = Triangle(vertici=(punto.posizione, vertice_c.posizione, vertice_a.posizione), surface=finestra,
                                 visibile=False)

        area_totale = triangolo_pab.area + triangolo_pbc.area + triangolo_pca.area

        if int(area_totale) == int(triangolo_origine.area):
            vertice_casuale: Point = choice((vertice_a, vertice_b, vertice_c))
            punto_medio_pos = data.utils.calcola_punto_medio(punto.posizione, vertice_casuale.posizione)

            Point(posizione=punto_medio_pos, surface=finestra, visibile=True)

        pygame.display.update()
        # clock.tick(240)


if __name__ == '__main__':
    main()
